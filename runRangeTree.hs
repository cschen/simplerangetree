{-# LANGUAGE ScopedTypeVariables #-}

-- Expected input:
-- first line: MAX_X
-- following lines:
--   Q a b (0 <= a < b <= MAX_X) --> [a,b) sum
--   U a x (0 <= a < MAX_X) --> update v[a] to x

import Control.Monad.State
import qualified Data.ByteString.Lazy.Char8 as B
import Data.Maybe (fromJust)
import System.IO

import RangeTree

-- I/O handling

bsToInt :: B.ByteString -> Int
bsToInt = fst . fromJust . B.readInt

lineToOperation :: B.ByteString -> RangeTreeOperation
lineToOperation s =
  let
    (c,str) = fromJust $ B.uncons s
    operation = case c of
      'Q' -> Query
      'U' -> Update
      otherwise -> error "Unrecognised operation"
    v1:v2:_ = map bsToInt $ B.words str
  in
    operation v1 v2

main = do
  f <- B.hGetContents stdin
  let line1:lines = B.lines f
  let width::Int = bsToInt line1
  let operations::[RangeTreeOperation] = map lineToOperation lines
  print width
  let results = evalState (doOperations operations) (newRangeTree width)
  mapM_ print results

