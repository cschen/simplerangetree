{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Test.Framework (Test, defaultMain, testGroup)
import Test.Framework.Providers.QuickCheck2 (testProperty)
import Test.QuickCheck
import qualified Test.QuickCheck.Property as P

import Control.Monad (liftM2)
import Control.Monad.State
import Data.List (nubBy)
import RangeTree

main :: IO () 
main = defaultMain tests

tests :: [Test]
tests = [
    testProperty "Correct empty sum" correctEmptySum
  , testProperty "Correct sum at single point" correctPointSum
  , testProperty "Correct range sum" correctRangeSum
  ]

instance Arbitrary RangeTreeOperation where
  shrink (Update x v) = [Update x' v' | x' <- shrink x, v' <- shrink v]
  shrink (Query x y) = [Query x' y' | x' <- shrink x, y' <- shrink y]

arbitraryOp w = oneof [arbitraryUpdate w, arbitraryQuery w]

arbitraryUpdate w = liftM2 Update a' a'
  where a' = choose (0, w-1)
arbitraryQuery w = do
  (x, y) <- chooseOrderedPair `suchThat` (\(x,y) -> 0 <= x && x <= y && y <= w)
  return $ Query x y
  where chooseOrderedPair = liftM2 (,) (choose (0,w-1)) (choose (0,w-1))

correctEmptySum :: Gen Bool
correctEmptySum = do
  (w, ops, tree) <- initialiseGenericCase
  x <- choose (0,w-1)
  return $ query tree x x == 0

correctPointSum :: Gen P.Result
correctPointSum = do
  (w, ops, tree) <- initialiseGenericCase
  x <- choose (0, w-1)
  return $ (P.liftBool $ query tree x (x+1) == last ((0:) $ filterUpdatesTo x ops))
    {P.reason = "w = " ++ show w ++ "\n operations = " ++ show ops ++ "x = " ++ show x}
  where
    filterUpdatesTo x ops = map (\(Update _ v) -> v) $
      filter (\op -> case op of
        (Update x' _) -> x == x'
        otherwise -> False
      ) ops

correctRangeSum :: Gen P.Result
correctRangeSum = do
  (w, ops, tree) <- initialiseGenericCase
  q@(Query x1 x2) <- arbitraryQuery w
  return $ (P.liftBool $ query tree x1 x2 ==
    sumV (nubBy samePt $ filter (inRange x1 x2) $ reverse ops))
    {P.reason = "w = " ++ show w ++ "\n operations = " ++ show ops ++ "q = " ++ show q}
  where
    sumV = sum . map (\(Update _ v) -> v)
    samePt (Update x _) (Update y _) = x == y
    inRange x1 x2 op = case op of
      Update x _ -> x1 <= x && x < x2
      otherwise -> False

initialiseGenericCase :: Gen (Int, [RangeTreeOperation], RangeTree)
initialiseGenericCase = do
  w::Int <- choose (1, 1000000)
  ops <- listOf $ arbitraryOp w
  let tree = execState (doOperations ops) $ newRangeTree w
  return (w, ops, tree)

withinBounds :: Int -> RangeTreeOperation -> Bool
withinBounds w (Update x _) = 0 <= x && x < w
withinBounds w (Query x y) = 0 <= x && x < y && y <= w
