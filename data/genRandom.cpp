#include <cstdio>
#include <cstdlib>
#include <algorithm>

#define MAX_V 1000

int main(int argc, char** argv) {
  if (argc <= 2) {
    printf("Usage: %s [width] [nOperations]\n", argv[0]);
  }

  int width, nOperations;
  sscanf(argv[1], "%d", &width);
  sscanf(argv[2], "%d", &nOperations);

  printf("%d\n", width);
  for (int i = 0; i < nOperations; i++) {
    if (rand()%2) {
      printf("U %d %d\n", rand()%width, rand()%MAX_V);
    } else {
      int a = rand()%width;
      int b = rand()%width;
      if (a > b) std::swap(a,b);
      printf("Q %d %d\n", a, b);
    }
  }
}
